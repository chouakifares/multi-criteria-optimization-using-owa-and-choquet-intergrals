from ProjectSelection import *
from utils import *
import matplotlib.pyplot as plt
from sys import argv
from time import time
from tqdm import tqdm
import numpy as np

def study_n_impact(max_n, p):
    times = []
    all_times = []
    for i in tqdm(range(1,max_n+1)):
        for k in range(100):
            t = time()
            pr = ProjectSelectionProblem.generate_instance(i,p, additivity = i)
            times.append(time() - t)

        all_times.append(sum(times)/len(times))
    plt.subplot(1,2,1)
    plt.plot([i for i in range(1,max_n+1)], all_times)
    plt.grid(alpha=0.25)
    plt.xlabel("nombre de critère d'évaluation des projets")
    plt.ylabel("temps d'execution en seconde")
    plt.xticks([i for i in range(1, max_n+1)])
    plt.subplot(1,2,2)
    plt.plot([i for i in range(1,max_n+1)], np.log(all_times))
    plt.grid(alpha=0.25)
    plt.xticks([i for i in range(1, max_n+1)])
    plt.xlabel("nombre de critère d'évaluation des projets")
    plt.ylabel("log du temps d'execution en seconde")
    plt.show()


def study_p_impact(max_p, n):
    times = []
    all_times = []
    for i in tqdm(range(5,max_p+1)):
        for k in range(100):
            t = time()
            pr = ProjectSelectionProblem.generate_instance(n,i, additivity = n)
            times.append(time() - t)

        all_times.append(sum(times)/len(times))
    
    plt.xlabel("nombre de projets sur lesquels se fait le choix")
    plt.ylabel("Temps d'execution en seconde")
    plt.grid(alpha=0.25)
    plt.plot([i for i in range(5,max_p+1)], all_times)
    plt.show()

param, max_value, other_param_value = argv[1], int(argv[2]), int(argv[3])
if(param =="n"):
    study_n_impact(max_value, other_param_value)
elif(param=="p"):
    study_p_impact(max_value, other_param_value)
