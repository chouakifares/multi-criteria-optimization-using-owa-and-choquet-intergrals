# multi-criteria optimization using OWA and Choquet intergrals

## How to run
- create virtual environement `python3 -m venv temp_env`
- launch your virtual environement `source temp_env/bin/activate`
- install requirements `pip install -r requirements.txt`
- launch each file using the command `python3 <filename> <arg_list>`     

## Contents of the project
In addition to this file the project contains 13 .py files, one .txt file and one directory. The contents of each of these files is described in the following

- `FairDivision.py`: File that contains the class defining the fair division of undivisible goods problem and several methods that can be used to solve it.

- `ProjectSelection.py`: File that contains the class defining the multicriteria projects selection problem and a method that allows to solve it using the Choquet integral.

- `RobustPathFinding.py`: File that contains the class defining the problem of path finding under uncertainty and implements a method for solving this problem using the the Choquet integral.

- `question1_1.py`: File that answers question 1.1 of the project and allows user to visualize the several assignements returned by OWA with respect to the value of the parameter &alpha;. This file can be ran by executing the command `python3 question1_1.py` with no arguments.

- `question1_2.py`: File that answers question 1.2 of the project and allows user to reproduce the experiment used to benchmark OWA solution time with respect to the number of agents in the Fair share problem. This file can be ran by executing the command `python3 question1_2.py` with no arguments.

- `question1_3.py`: File that answers question 1.3 of the project and allows user to visualize the solutions returned by WOWA when varying the weight of a particular agent. This file can be ran by executing the command `python3 question1_3.py` with no arguments.

- `question1_4.py`: File that answers question 1.4 of the project and allows user to reproduce the experiment used to benchmark WOWA with respect to the number of agent. This file can be ran by executing the command `python3 question1_4.py` with no arguments.

- `question2_2.py`: File that anwers question 2.2 of the project and allows user to get a solution to the problem of multicriteria project selection and test other capacities that will allow them to get solutions that are different from the one  provided by the optimization of a weighted sum of the criterias. This file can be ran by executing the command `python3 question2_2.py a b c` where a, b and c represent the mobius masses for the sets `{1st_criterion}`, `{2nd_criterion}`, `{1st_criterion, 2nd_criterion}`.

- `question2_3.py`: File that anwers question 2.3 of the project and allows user to get the time it takes for the LP to solve an instance with `n` evaluation criterias and `p` projects. This file can be ran by executing the command `python3 question2_3.py n p` where `n` is the number of evaluation criterias and `p` the number of projects.  

- `question2_3bis.py`: File that anwers question 2.3 of the project and allows user to reproduce the experiment used to benchmark the LP used to solve the tackled problem. This file can be ran by executing the command `python3 question2_3bis.py param max_value other_param_value` where `param` is the parameter to vary (either `n` or `p`), `max_value` is the maximum value that the `param` is allowed to reach and  `other_param_value` is the value of the other parameter that will be fixed throughout the whole experiment.  
- `question3.py`: File that answers question 3 of the project and allows the user to see what is the best itinerary to take with respect to the capacity they're using by solving a Choquet Integral.This file can be ran by executing the command `python3 question3.py a b c` where a, b and c represent the mobius masses for the sets `{1st_criterion}`, `{2nd_criterion}`, `{1st_criterion, 2nd_criterion}`.
- logs: Directory that contains the results of the experiements of `question1_2.py` and `question1_4.py`.
- `generate_plots.py`: File that allows to generate plots from the results of experiments done in `question1_2.py` and `question1_4.py`. In order to run this file you need to execute the command `python3 generate_plots logs/[question1_2|question1_4]`.
- `utils.py`: File containing the definition of the BeliefFunction class and a function that can be used to generate the weights for OWA.
