from RobustPathFinding import *
from utils import *
import matplotlib.pyplot as plt
from sys import argv

def print_solution(sol, value):
    print("solution retournée lors de l'optimisation de la moyenne des critère est:", end="")
    for i in range(len(sol)-1):
        print(f"{sol[i]}-->",sep="", end="")
    print(sol[-1], end="")
    print(f", cette solution est de valeur {value}")

adjacency_matrix = np.array([
    [0,1,1,1,0,0,0],
    [0,0,1,1,1,0,0],
    [0,0,0,0,1,1,0],
    [0,0,1,0,0,1,0],
    [0,0,0,0,0,0,1],
    [0,0,0,0,0,0,1],
    [0,0,0,0,0,0,0]
])
costs_tensor = np.array([
    [
        [0,5,10,2,0,0,0],
        [0,0,4,1,4,0,0],
        [0,0,0,0,3,1,0],
        [0,0,1,0,0,3,0],
        [0,0,0,0,0,0,1],
        [0,0,0,0,0,0,1],
        [0,0,0,0,0,0,0]
    ],
    [
        [0,3,4,6,0,0,0],
        [0,0,2,3,6,0,0],
        [0,0,0,0,1,2,0],
        [0,0,4,0,0,5,0],
        [0,0,0,0,0,0,1],
        [0,0,0,0,0,0,1],
        [0,0,0,0,0,0,0]
    ]
])
bl = BeliefFunction(2, {(0,):0.5, (1,):0.5, (0,1):0}, 2)
pr = RobustPathFindingProblem(7, adjacency_matrix, costs_tensor,bl)
print_solution(*pr.solve())

a,b,c = float(argv[1]), float(argv[2]), float(argv[3])
bl = BeliefFunction(2, {(0,):a, (1,):b, (0,1):c}, 2)
pr = RobustPathFindingProblem(7, adjacency_matrix, costs_tensor,bl)
print_solution(*pr.solve())
