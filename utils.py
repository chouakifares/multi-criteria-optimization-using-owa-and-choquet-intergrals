import numpy as np
from itertools import combinations

def generate_weights(n, alpha):
    w = []
    for i in range(1,n+1):
        left_term = ((n-i+1)/n)**alpha
        right_term = ((n-i)/n)**alpha
        w.append(left_term - right_term)
    return w

class BeliefFunction:
    def __init__(self, n, mobius, additivity):
        self.n = n
        self.mobius = mobius
        self.additivity = additivity

    @classmethod
    def generate_belief_function(cls, n, additivity):
        if(additivity > n):
            raise Exception(f"""Belief function for set with {n} modalities
                can't have additivity {additivity}""")
        mobius = {}

        masses = np.random.dirichlet([
            1
            for i in range(1, additivity+1)
            for j in combinations(range(n), i)
        ])
        ind = 0
        for i in range(1,additivity+1):
            for c in combinations(range(n), i):
                mobius[c] = masses[ind]
                ind += 1

        return BeliefFunction(n, mobius, additivity)
