from cplex import *
from docplex.mp.model import Model
import numpy as np
import random
from itertools import combinations
from utils import *

class ProjectSelectionProblem:
    def __init__(self, n, p, utility_matrix, costs,belief_function):
        if(utility_matrix.shape != (n,p)):
            raise Exception(
                f"""
                    Utility matrix of size {utility_matrix.shape}
                    for problem with {n} agents and {p} objects"""
            )
        #number of agents
        self.n = n
        #number of objects
        self.p = p
        #utility of giving an objet to an agent
        self.utility_matrix = utility_matrix
        #costs of each projects
        self.costs = costs
        #belief function that is used as a capacity
        self.belief_function = belief_function


    @classmethod
    def generate_instance(cls, n, p, additivity,unitary_sat_threshold = 20, unitary_cost_threshold = 100):
        matrix = np.random.randint(low=0, high=unitary_sat_threshold, size=(n,p))
        belief_function = BeliefFunction.generate_belief_function(n,additivity)
        costs = np.random.randint(low=0, high=unitary_cost_threshold, size=(p,))
        return ProjectSelectionProblem(n,p,matrix, costs,belief_function)

    def choquet_solve(
        self
    ):
        m = Model()
        z_vars,y_vars = {},{}
        for j in range(self.p):
            z_vars[f"z_{j}"] = m.binary_var(name = f"z_{j}")
        m.add_constraint(
            sum([
                self.costs[i] * z_vars[f"z_{i}"] for i in range(self.p)
            ]) <= sum(self.costs)/2
        )
        objective = 0
        for a in self.belief_function.mobius:
            y_vars[a] = m.continuous_var(name=f"y_{a}")
            objective += self.belief_function.mobius[a] * y_vars[a]
            for i in a:
                m.add_constraint(y_vars[a] <=  sum([
                        z_vars[f"z_{j}"] * self.utility_matrix[i][j]
                            for j in range(self.p)
                    ])
                )
        m.maximize(objective)
        m.export("test.lp")
        sol = m.solve()
        assignment = []
        assignment_value = [0 for i in range(self.n)]
        for i in range(self.p):
            if sol.get_value(f"z_{i}"):
                assignment.append(1)
                for j in range(self.n):
                    assignment_value[j] += self.utility_matrix[j][i]
            else:
                assignment.append(0)
        return assignment_value, assignment
