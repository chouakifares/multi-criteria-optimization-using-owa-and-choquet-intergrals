from cplex import *
from docplex.mp.model import Model
import numpy as np
import random
from itertools import combinations

class RobustPathFindingProblem:
    def __init__(self,nb_vertices, adjacency_matrix, costs_tensor, belief_function):
        self.nb_vertices = nb_vertices
        self.adjacency_matrix = adjacency_matrix
        self.costs_tensor = costs_tensor
        self.belief_function = belief_function

    def solve(self):
        m = Model()
        z_vars,y_vars = {},{}
        for i in range(self.nb_vertices):
            for j in range(self.nb_vertices):
                if(self.adjacency_matrix[i][j] == 1):
                    z_vars[f"z_{i}_{j}"] = m.binary_var(name = f"z_{i}_{j}")
        for i in range(1,self.nb_vertices-1):
            m.add_constraint(
                sum([z_vars[f"z_{i}_{j}"] for j in range(self.nb_vertices) if f"z_{i}_{j}" in z_vars.keys()])
                == sum([z_vars[f"z_{j}_{i}"] for j in range(self.nb_vertices) if f"z_{j}_{i}" in z_vars.keys()])
            )
        m.add_constraint(
            sum([
                    z_vars[f"z_{j}_{self.nb_vertices-1}"]
                        for j in range(self.nb_vertices)
                        if f"z_{j}_{self.nb_vertices-1}" in z_vars.keys()
                ])==1
        )

        objective = 0
        for a in self.belief_function.mobius:
            y_vars[a] = m.continuous_var(name=f"y_{a}", lb = -np.infty)
            objective += self.belief_function.mobius[a] * y_vars[a]
            for i in a:
                path_cost = 0
                for u in range(self.nb_vertices):
                    for v in range(self.nb_vertices):
                        if(f"z_{u}_{v}" in z_vars.keys()):
                            path_cost-= z_vars[f"z_{u}_{v}"] * self.costs_tensor[i][u][v]
                m.add_constraint(y_vars[a] <= path_cost)

        m.maximize(objective)
        m.export("test.lp")
        sol = m.solve()
        tmp = []
        for s in range(self.nb_vertices):
            for t in range(self.nb_vertices):
                if(f"z_{s}_{t}" in z_vars and sol.get_value(f"z_{s}_{t}")):
                    tmp.append((chr(s+97),chr(t+97)))
        to_return = [tmp[0][0]]
        while(len(tmp)):
            for i in range(len(tmp)):
                if(tmp[i][0]==to_return[-1]):
                    to_return.append(tmp[i][1])
                    del tmp[i]
                    break
        return to_return, (sol.get_value("y_(0,)"), sol.get_value("y_(1,)"))
