from cplex import *
from docplex.mp.model import Model
import numpy as np
from itertools import combinations


class FairDivisionProblem:
    def __init__(self, n, p, utility_matrix):
        if(utility_matrix.shape != (n,p)):
            raise Exception(
                f"""
                    Utility matrix of size {utility_matrix.shape}
                    for problem with {n} agents and {p} objects"""
            )
        #number of agents
        self.n = n
        #number of objects
        self.p = p
        #utility of giving an objet to an agent
        self.utility_matrix = utility_matrix


    @classmethod
    def generate_instance(cls, n, p, unitary_sat_threshold = 20):
        matrix = np.random.randint(low=0, high=unitary_sat_threshold, size=(n,p))
        return FairDivisionProblem(n,p,matrix)

    def general_owa_solve(self,w):
        for i in range(self.n):
            y_vars[f"y_{i}"] = m.binary_var(name = f"y_{i}")
            for j in range(self.p):
                b_vars[f"b_{i}_{j}"] = m.binary_var(name = f"b_{i}_{j}", lb = 0)

        objective = sum([y_vars[i] * w_[i] for i in range(self.n)])


    @classmethod
    def generate_lorenz_weights(cls,w):
        w_prim = []
        for i in range(len(w)-1):
            w_prim.append(w[i] - w[i+1])
        w_prim.append(w[len(w)-1])
        return w_prim

    #Attempt at using the Ogryzac linearisation
    def fairness_owa_solve(self,
        w,
        nb_min_objects_for_agent,
        nb_max_objects_for_agent
    ):
        if(len(w) != self.n):
            raise Exception(
                f"""weights vector of size {len(w)}
                    for problem with {n} agents
                """
            )
        m = Model()
        #m.context.solver.log_output = True
        z_vars,r_vars, b_vars = {},{},{}
        for i in range(self.n):
            r_vars[f"r_{i}"] = m.continuous_var(name = f"r_{i}", lb = 0)
            for j in range(self.p):
                z_vars[f"z_{i}_{j}"] = m.binary_var(name = f"z_{i}_{j}")
                b_vars[f"b_{i}_{j}"] = m.continuous_var(name = f"b_{i}_{j}", lb = 0)

        #each object is assigned to exactly one agent
        for j in range(self.p):
            m.add_constraint(
                sum([z_vars[f"z_{i}_{j}"] for i in range(self.n)])==1
            )

        #each agent is gets at least nb_min_objects_for_agent
        #and at most nb_max_object_for_agent
        for i in range(self.n):
            m.add_constraint(
                sum([z_vars[f"z_{i}_{j}"] for j in range(self.p)])
                    >=nb_min_objects_for_agent
            )
            m.add_constraint(
                sum([z_vars[f"z_{i}_{j}"] for j in range(self.p)])
                    <=nb_max_objects_for_agent
            )
        max_ = m.continuous_var(name="max_")
        for i in range(self.n):
            yi = sum(
                z_vars[f"z_{i}_{j}"] * self.utility_matrix[i][j]
                    for j in range(self.p)
            )
            m.add_constraint(max_ >= yi)
            for k in range(self.n):
                m.add_constraint(
                    r_vars[f"r_{k}"] + b_vars[f"b_{i}_{k}"] >= max_ - yi
                )
        w_prim = FairDivisionProblem.generate_lorenz_weights(w)
        objective = 0

        for k in range(self.n):
            objective += (
                w_prim[k]*(max_ * (k+1) - (k+1)*r_vars[f"r_{k}"]
                - sum([b_vars[f"b_{i}_{k}"] for i in range(self.n)]))
            )

        m.maximize(objective)
        #set timelimit of 10 minutes
        m.parameters.timelimit=600;
        sol = m.solve()
        if(sol is not None):
            assignment_value = [0 for i in range(self.n)]
            for i in range(self.n):
                for j in range(self.p):
                    if(sol.get_value(f"z_{i}_{j}")):
                        assignment_value[i] += self.utility_matrix[i][j]
            lorenz_vec = [self.lorenz_values(assignment_value,i+1) for i in range(self.n)]

            return lorenz_vec, assignment_value, sol.objective_value, m.solve_details.mip_relative_gap
        else:
             None, None, None, m.solve_details.mip_relative_gap


    def lorenz_values(self, vector, rank):
        m = Model()
        b_vars = {}
        max_ = max(vector)
        r = m.continuous_var(name=f"r_{rank-1}")
        for i in range(self.n):
            b_vars[f"b_{i}_{rank-1}"] = m.continuous_var(
                name=f"b_{i}_{rank-1}"
            )
            m.add_constraint(r + b_vars[f"b_{i}_{rank-1}"] >= max_-vector[i])

        m.maximize(max_ * rank - (rank*r + sum([i for i in b_vars.values()])))
        sol = m.solve()
        return sol.objective_value


    #this can be used to optimize an owa if you give it uniforally distributed agent weights
    # and a phi_function that is a piece-wise function of your owa weights
    def wowa_solve(self,
        agent_weights,
        phi_func,
        nb_min_objects_for_agent,
        nb_max_objects_for_agent):
        capacity = {}
        #defining the capacity as the sum of the agent weights for the
        for i in range(self.n+1):
            for c in combinations(range(self.n), i):
                capacity[c] = phi_func(sum(agent_weights[j] for j in c))
        return self.choquet_solve(
            capacity,
            nb_min_objects_for_agent,
            nb_max_objects_for_agent
        )

    def choquet_solve(
        self,
        capacity,
        nb_min_objects_for_agent,
        nb_max_objects_for_agent
    ):
        m = Model()
        z_vars,d_vars = {},{}
        for i in range(self.n):
            for j in range(self.p):
                z_vars[f"z_{i}_{j}"] = m.binary_var(name = f"z_{i}_{j}")
        objective = 0
        for a in capacity:
            d_vars[a] = m.continuous_var(name=f"d_{a}")
            objective += capacity[a] * d_vars[a]
        m.maximize(objective)

        for i in range(self.n):
            m.add_constraint(
                sum([d_vars[a] for a in d_vars if i in a])
                <= sum(
                    [self.utility_matrix[i][j] * z_vars[f"z_{i}_{j}"]
                    for j in range(self.p)]
                )
            )

        #each object is assinged to exactly one agent
        for j in range(self.p):
            m.add_constraint(
                sum([z_vars[f"z_{i}_{j}"] for i in range(self.n)])==1
            )

        #each agent is gets at least nb_min_objects_for_agent
        #and at most nb_max_object_for_agent
        for i in range(self.n):
            m.add_constraint(
                sum([z_vars[f"z_{i}_{j}"] for j in range(self.p)])
                    >=nb_min_objects_for_agent
            )
            m.add_constraint(
                sum([z_vars[f"z_{i}_{j}"] for j in range(self.p)])
                    <=nb_max_objects_for_agent
            )
        m.context.solver.log_output = False
        m.parameters.timelimit=600;
        m.export("test.lp")
        assignement_matrix= np.zeros((self.n, self.p))
        sol = m.solve()
        if(sol is not None):
            assignment_value = [0 for i in range(self.n)]
            for i in range(self.n):
                for j in range(self.p):
                    if sol.get_value(f"z_{i}_{j}"):
                        assignment_value[i] += self.utility_matrix[i][j]
                        assignement_matrix[i,j] = 1
            return assignement_matrix,assignment_value, [
                self.lorenz_values(assignment_value, i+1)
                    for i in range(len(assignment_value))
                ], m.solve_details.mip_relative_gap
        else:
            return None,None, None, m.solve_details.mip_relative_gap
