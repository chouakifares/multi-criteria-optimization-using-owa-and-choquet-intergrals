from ProjectSelection import *
from utils import *
import matplotlib.pyplot as plt
from sys import argv

utility_matrix = np.array([
    [19, 6, 17, 2],
    [2, 11, 4, 18]
])

n = 2
p = 4
costs = [40,50,60,50]
a,b,c = float(argv[1]), float(argv[2]), float(argv[3])
bl = BeliefFunction(2, {(0,):0.5, (1,):0.5, (0,1):0}, 2)
pr = ProjectSelectionProblem(n, p, utility_matrix, costs, bl)
value, assignement = pr.choquet_solve()
print("solution retournée lors de l'optimisation de la moyenne des critère",assignement)
print("valeur de la solution retournée lors de l'optimisation de la moyenne des critère",value)
print()
bl = BeliefFunction(2, {(0,):0, (1,):0, (0,1):1}, 2)
pr = ProjectSelectionProblem(n, p, utility_matrix, costs, bl)
value, assignement = pr.choquet_solve()
print("solution retournée lors de l'utilisation d'une capacité avec m({1,2}) = 1",assignement)
print("valeur de la solution retournée lors de l'utilisation d'une capacité avec m({1,2}) = 1",value)
print()
a,b,c = float(argv[1]), float(argv[2]), float(argv[3])
bl = BeliefFunction(2, {(0,):a, (1,):b, (0,1):c}, 2)
pr = ProjectSelectionProblem(n, p, utility_matrix, costs, bl)
value, assignement = pr.choquet_solve()
print("solution retournée lors de l'utilisation de la capacité fournie en paramètre",assignement)
print("valeur de la solution retournée lors de l'utilisation de la capacité fournie en paramètre",value)
