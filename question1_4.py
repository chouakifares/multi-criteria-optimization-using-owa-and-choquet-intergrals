from FairDivision import *
from utils import *
import matplotlib.pyplot as plt
from time import time
from sys import argv
from tqdm import tqdm
import numpy as np

times = []
gaps = []
all_times = []
for i in [5,10,15]:
    times = []
    for K in tqdm(range(10)):
        p = FairDivisionProblem.generate_instance(
                i,
                5*i
            )
        t = time()
        alpha = np.random.randint(5, 20)
        _,_,_,g = p.wowa_solve(np.random.dirichlet([1 for i in range(i)]), lambda x: x**5,0,i*5)
        times.append(time()-t)
    times= np.array(times)
    np.savetxt(f"logs/question1_4/{i}_times.csv",times, delimiter=",")
    all_times.append(times)
