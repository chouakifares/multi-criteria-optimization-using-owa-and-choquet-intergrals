from FairDivision import *
from utils import *
import matplotlib.pyplot as plt
utility_matrix = np.array(
    [
        [12, 20, 6, 5, 8],
        [5, 12, 6, 8, 5],
        [8, 5, 11, 5, 6],
        [6, 8, 6, 11, 5],
        [5, 6, 8, 7, 7],
    ]
)
n = 5
p = 5

p = FairDivisionProblem(n,p,utility_matrix)
delta = 1/25
x = np.arange(5)

results = []
for i in range(5):
    current_vec = [1/5 for i in range(5)]
    #tmp = []
    for k in range(6):
        _,assignment_value_2, lorenz_vec_2, gap = p.wowa_solve(current_vec,lambda x: x**2, 1,1)
        _,assignment_value_5, lorenz_vec_5, gap = p.wowa_solve(current_vec,lambda x: x**5, 1,1)
        plt.subplot(2,3,k+1)
        plt.grid(alpha = 0.5)

        plt.bar(x-0.1,height = assignment_value_2, width = 0.2, label = "$α = 2$", color ="#b434eb")
        plt.bar(x+0.1,height = assignment_value_5, width = 0.2, label = "$α = 5$", color="#9fe089")
        plt.legend()
        plt.tight_layout(pad=0.1)

        plt.ylim(0,21)
        plt.yticks([2*i for i in range(11)])
        #tmp.append(assignment_value)
        plt.title(f"Valeur de l'affectations trouvée avec w = {[abs(round(i,2)) for i in current_vec]}")

        current_vec[i] += 4* delta
        current_vec = [
            current_vec[j] - delta if j != i else current_vec[j]
                for j in range(len(current_vec))
        ]
    #results.append(tmp)
    plt.subplots_adjust(hspace=0.2, wspace=0.05)
    plt.show()
