from FairDivision import *
from utils import *
import matplotlib.pyplot as plt

utility_matrix = np.array(
    [
        [12, 20, 6, 5, 8],
        [5, 12, 6, 8, 5],
        [8, 5, 11, 5, 6],
        [6, 8, 6, 11, 5],
        [5, 6, 8, 7, 7],
    ]
)
n = 5
p = 5

p = FairDivisionProblem(n,p,utility_matrix)
lorenz, assignments = [], []

_,tmp_assignment, tmp_lorenz,_ = p.wowa_solve([1/5 for i in range(n)], lambda x: x**1,0,1)
lorenz.append(tmp_lorenz)
assignments.append(tmp_assignment)

_,tmp_assignment, tmp_lorenz,_ = p.wowa_solve([1/5 for i in range(n)], lambda x: x**2,0,1)
lorenz.append(tmp_lorenz)
assignments.append(tmp_assignment)



_,tmp_assignment, tmp_lorenz,_ = p.wowa_solve([1/5 for i in range(n)], lambda x: x**7,0,1)
lorenz.append(tmp_lorenz)
assignments.append(tmp_assignment)


x = np.arange(5)

plt.subplot(1,2,1)
plt.ylim(0,60)
plt.bar(x-0.3,height = lorenz[0], width = 0.2, label = "$α = 1$", alpha=0.8)
plt.bar(x-0.1,height = lorenz[1], width = 0.2, label = "$6 ≥ α ≥ 2$", alpha=0.8)
plt.bar(x+0.1,height = lorenz[2], width = 0.2, label = "$α ≥ 7$", alpha=0.8)
plt.legend()
plt.grid(alpha=0.9)
plt.title("Vecteur de Lorenz")
plt.yticks([5*i for i in range(12)])
plt.xticks(range(5), [f"composante {i+1}" for i in range(5)])

plt.subplot(1,2,2)
plt.ylim(0,24)
plt.bar(x-0.3,height = assignments[0],width = 0.2, label = "$α = 1$", alpha=0.8)
plt.bar(x-0.1,height = assignments[1],width = 0.2, label = "$6 ≥ α ≥ 2$", alpha=0.8)
plt.bar(x+0.1,height = assignments[2],width = 0.2, label = "$α ≥ 7$", alpha=0.8)
plt.legend()
plt.yticks([2*i for i in range(12)])
plt.xticks(range(5), [f"sat de l'agent {i+1}" for i in range(5)])

plt.title("Valeurs des affectations ")
plt.grid(alpha=0.9)
plt.tight_layout(pad=2)
plt.suptitle("Affectations trouvées pour les differentes valeurs de $α$")
plt.show()
