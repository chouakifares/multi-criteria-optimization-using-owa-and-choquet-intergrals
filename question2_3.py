from ProjectSelection import *
from utils import *
import matplotlib.pyplot as plt
from sys import argv
from time import time

n, p = int(argv[1]), int(argv[2])
times = []
for k in range(100):
    t = time()
    pr = ProjectSelectionProblem.generate_instance(n,p, additivity = n)
    times.append(time() - t)

print(sum(times)/len(times))
