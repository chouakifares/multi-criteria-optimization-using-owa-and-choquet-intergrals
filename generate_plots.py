import matplotlib.pyplot as plt
import pandas as pd
import os 
from sys import argv
import re 
import numpy as np

path = argv[1]
values = {}
for filename in os.listdir(path):
    if filename.endswith('.csv'):
        file_path = os.path.join(path, filename)
        values[int(re.match(r"[0-9]+", filename).group())] = pd.read_csv(file_path).mean()
values = dict(sorted(values.items()))
plt.subplot(1,2,1)
plt.grid(alpha=0.25)
plt.plot(values.keys(), values.values())
plt.xticks(list(values.keys()))
plt.xlabel("Nombre d'agent dans l'instance")
plt.ylabel("Temps d'execution en seconde")
plt.subplot(1,2,2)
plt.xlabel("Nombre d'agent dans l'instance")
plt.ylabel("log du temps d'execution en seconde")
plt.xticks(list(values.keys()))
plt.grid(alpha = 0.25)
plt.suptitle("Evolution du temps d'execution en fonction de la taille de l'instance (s)")
plt.plot(values.keys(), [np.log(i) for i in values.values()])
plt.show()
