from FairDivision import *
from utils import *
import matplotlib.pyplot as plt
from time import time
from sys import argv
from tqdm import tqdm
gaps = []
all_times = []
for i in [5,10,15]:
    times = []
    for K in tqdm(range(10)):
        p = FairDivisionProblem.generate_instance(
                i,
                5*i
            )
        t = time()
        _,_,_,_ = p.wowa_solve([1/5 for j in range(i)], lambda x: x**np.random.randint(2,20),5,5)
        times.append(time()-t)
    times= np.array(times)
    #np.savetxt(f"logs/question1_2/{i}_times.csv",times, delimiter=",")
    all_times.append(times)

all_times = np.array(all_times)


plt.xlabel("Nombre d'agent dans l'instance")
plt.ylabel("Temps d'execution en seconde")
plt.plot([i for i in range(5,n+1)], np.mean(all_times, axis = 1))
plt.fill_between([i for i in range(5,n+1)], np.percentile(all_times,10,axis = 1),np.percentile(all_times,90,axis = 1), alpha = 0.5)
plt.grid(alpha = 0.25)
plt.show()

plt.xlabel("Nombre d'agent dans l'instance")
plt.ylabel("log Temps d'execution en seconde")
plt.plot([i for i in range(5,n+1)], np.log(np.mean(all_times, axis = 1)))
plt.grid(alpha = 0.25)
plt.show()
